---
layout: page
title: Session 1
permalink: /session1/
---
<iframe src="https://archive.org/embed/BRITTOListeningGroupSession1" width="500" height="30" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen></iframe>

This Listening Group is accompanied by the following texts:

* [Steve Goodman - Sonic Warfare: Sound, Affect, and the Ecology of Fear (2012)](https://archive.org/download/BRITTOListeningGroupSession1/Goodman%20-%202009%20-%20Sonic%20warfare.pdf)
* [R. Murray Schafer - The Soundscape: Our Sonic Environment and the Tuning of the World (1993)](https://archive.org/download/BRITTOListeningGroupSession1/Schafer%20-%201977%20-%20The%20tuning%20of%20the%20world.pdf)

In Goodman’s ‘Sonic Warfare’, we look at American Artist Mark Bain who “draws attention to the primacy of vibration in any discussion of sound, affect and power.” This opens out into considerations of “the built environment [as] frozen music”. Where “the concrete ripples and pulses with invisible vortical force fields. Objects become vectorial, simultaneously projectile and contagious, defying gravity, sliding across horizontal surfaces. The air becomes heavy and metal screams under the torque.”

By way of illustration we present Bill Fontana’s ‘Harmonic Bridge’ where the vibrational sounds of London’s Millennium Bridge are retransmitted and reconfigured within the Tate’s Turbine Hall.

Goodman goes on to connect this vibrational territory with “the vernacular seismology and sonic dominance practiced by the bass materialists of the musical diaspora of Jamaican sound system culture” (Aswad’s “Hey Jah Children”). It is worth reading Goodman’s chapter ‘1931: Rhythmanalysis’ and seeing what connections make sense to you.

Reading Schafer’s chapter on ‘Perception’ we quickly run through a set of tools one may consider in thinking through the aural environment (from field studies to recorded music). We speculate on how different cultures and societies hear differently. His main point is that 'Western’ culture has lost much of it’s sonological competence. This is motivated by an ecological view of the loss of sonic diversity and quiet in much the same way environmentalists talk of loos of 'habitat’ etc.

This privileging of a speculated past leaves me slightly uncomfortable. We can’t know how other civilisations 'heard’ and to make any value judgement that any one approach/culture is 'preferable’ seems a bit dodgy. The toolset he discusses is however still useful for discussing and considering the 'soundscape’.

Using these tools we consider a cultural difference in the classical musics of the West vs the East. Again terminology works against us. The suggestion is that “Arab music is noted for rhythm and melody, while that of Western Europe - at least over the last three hundred fifty years - has emphasised harmony and dynamics”. This suggestion, amongst dinner table discussions was always met with a little indignation and I believe that the implication that wasn’t appreciated is that non western music lacks dynamics. This of course isn’t the suggestion.

By way of contrast/comparison we have Kachi Kafi [Darbari Kanada, Dadra] by Hassanbhai Bacchubhai & Unknown Artist and Bihamzaban بی همزبان by Pouran پوران representing Indian Classical Music and Iranian pop. Compared with Faust by Wagner and Hibiki-Hana-Ma by Iannis Xenakis.

It becomes clearer that 'dynamics’ is meant technically rather than judgementally. There are of course dynamics (in the colloquial sense) in the Eastern examples but the dynamics operate much more strongly through the 'Horizontal’ where as the Western examples are very much 'Vertical’.

We then move to Kamruzzaman Shadhin’s 'Roots 1.1’ which is a field recording of conversation with a small and somewhat marginalised ethnic community in Bangladesh, and to a recording of a diesel boat engine supplied by Marlene Harles. In both of these examples we end up drawing various sociopolitics either through the sounds of 'community’ or through the politicisation of the boat engine as 'noise’ by the traditional boat makers whose livelihoods are threatened by this encroaching machinery. The politics of vibration if you will.

The B Regiment & SpaceGhost’s 'Niharika’ helps reconnect these various threads through creatively entwining field recordings of crickets with German influenced electronica and bass/beat explorations. This connects brilliantly with Burial’s 'Near Dark’ where the bass sounds echo architectural sub-sonics and the city reconfigures as a sparse, haunting dub.

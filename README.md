# Britto-Listening-Group

From September to October 2015 I was the recipient of a Gasworks Fellowship residency with Britto Arts Trust, Dhaka, Bangladesh.

As part of my work I began a series of ‘Listening Groups’ with the intent of encouraging philosophical and personal discourses around sound and music - both in a Fine Arts context and where they intermingle with ‘popular music’.

Unfortunately, following several acts of violence and security advice we received, I left Bangladesh early. Rather than give up on the listening groups completely I concluded my work remotely utilising Facebook.

There are impracticalities with this kind of social network, not least it is mostly a closed system. I have transferred the content here then to act as a public archive and to facilitate greater sharing amongst any who wish to do so.

The here mentioned above was a combination of tumblr and mixcloud. I have moved it again though using github pages and archive.org.

https://mroystonward.gitlab.io/Britto-Listening-Group/

https://archive.org/details/@mroystonward

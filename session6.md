---
layout: page
title: Session 6
permalink: /session6/
---
<iframe src="https://archive.org/embed/BRITTOListeningGroupSession6" width="500" height="30" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen></iframe>

This Listening Group is accompanied by the following text:

* [Jacques Attali - Noise: The Political Economy of Music (1977)](https://archive.org/download/BRITTOListeningGroupSession6/Attali%20-%201989%20-%20Noise%20The%20Political%20Economy%20of%20Music.pdf)

Jacques Attali is an intriguing character, especially for such a discussion around sound and music. He is primarily an economist who was part of Mitterand’s socialist campaign group in the 70’s through to the French presidency in the 80s. He was key in a number of economic strategies and even initiated the international plan for relief as a result of Bangladesh’s flooding in 1989.

The Book ‘Noise’ is equally intriguing. It reads music very much in terms of Marxism and modes of production and theorises the ability such modes have in ‘announcing’ political revolutions.

He breaks down a history of music and it’s functions into 3 basic eras and adds a 4th speculative mode for the future.

These eras encompass 'Sacrificial’ ritual, 'Representational’ spectacle and 'Repetition’ via recording and broadcast technologies.

The future 'Post-Repeating’ theme he calls 'Composing’ and this is the context from which the text I have selected draws. It is important to note that Attali means 'Composing’ as a particular form of production rather than in the more traditional notion of 'writing’ musical scores etc.

“We are all condemned to silence - unless we create our own relation with the world and try to tie other people into the meaning we thus create. That is what composing is. Doing solely for the sake of doing…”

When Attali talks of making music solely for oneself he isn’t talking only about playing guitar by yourself in your bedroom. He is rather talking about labour and alienation and forms of production. Rather than musical production being for a specific goal, which makes the producer a tool and thus alienated from her production, he imagines production for its own sake which circumvents this alienation.

“Composition thus appears as a negation of the division of roles and labor as constructed by the old codes. Therefore, in the final analysis, to listen to music in the network of composition is to rewrite it: 'to put music into operation, to draw it towards an unknown praxis,’ […] The listener is the operator. Composition, then, beyond the realm of music, calls into question the distinction between worker and consumer…"

Taking Free Jazz as a case study the text traces a history of developments within Black Jazz music that held little commercial interest amongst the record labels which led to policies of only recording whites who sounded like blacks. To counter such censorships groups were formed such as the 'Jazz Composers Guild’ and AACM. They employed strategies, by pooling royalties and other resources, to self-support the development of these musical forms and sustainability of its practitioners. They set up distribution and production networks parallel to the capitalist music industry. These efforts never gained any great political power and ultimately subsided into more reflective activities.

Where it succeeded though, and what is more important in my own thinking, is that this meeting of black popular music and abstract European art music eliminated distinctions between popular and learned musics and broke down the repetitive hierarchy. Through refusing to go along with not being represented or heard, what Attali calls a 'crisis of proliferation’, they created 'local’ conditions to create new modes of production and a new music.

These 'local’ conditions can be seen elsewhere and in many ways, for me at least, correspond with the interrelations and explorations of values described by Small’s 'Musicking.’ DIY punk, Jamaican sound system, rave and pirate radio would be more recent examples. Sometimes these spill over into the more commercial industry that still clings to its last moments of relevancy and power but the mutations and developments continue.

My own background in music starts with Heavy Metal and moves, via experimental bands such as Sonic Youth, to improvisation and noise. Over the past 10 years I have moved more and more towards a group of musicians that is sometimes labelled as the 'no audience underground’.

The term 'No Audience Underground’ is rather tongue-in-cheek as this is a small community with little commercial interest and as a result generally very small audiences. It also can be read via the forms of production where most of the 'audience’ are also productive either as promoters, poster designers, other musicians etc. In fact these roles frequently shift and overlap. There are few, if any, passive receivers to our transmissions, there is no 'audience’, as everyone is involved in creating the scene. Even listeners are participants. There is a definite 'Do It Yourself’ attitude. Rather than this being a resistance to overpowering commercial cultural forms I think this is now more of an indifference to their irrelevance. This isn’t a case of doing it ourselves because the other networks wont support us, because we need to fight for our slice of the pie. Its more a case of doing it ourselves because that’s just what needs to be done, how else would it happen?

I wouldn’t go as far as to say the this 'No Audience Underground’ fulfils Attali’s predictions as they are underdeveloped and this was never an intentional thing. However it is possible to read this grouping, and other such movements, in light of such theories to develop new viewpoints and fuel critical debate.

It’s also very difficult to talk about something when you are a part of it, it’s difficult to get enough critical distance. If I had to sum it up though I’d say that it takes a radical view of folk music (as a form of production rather than as a genre or style) that also reaches out to other cultural forms from around the world, obsessive and obscurant record collecting, improvisatory practices (which are also deeply connected to radical views of folk art) and avant-garde music histories, and tries to develop it’s own modes of expression from within these influences.

Rather than try and describe this further I’ll let you focus upon listening.

I hope you have enjoyed these listening groups and I’d appreciate any comments. It’s a great shame I have had to finish these remotely and we couldn’t so easily enter into lively discussion and debate.
